﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GIT_Application.Startup))]
namespace GIT_Application
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
